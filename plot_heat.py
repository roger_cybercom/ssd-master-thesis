'''
Plot bounding box sizes saved in given csv file
Master thesis project Cybercom
Roger Kalliomaki
'''

import matplotlib.pyplot as plt
import numpy as np

# Set resolution for dataset
#REC = [1242,375] # KITTI 1242x375
#REC = [1914,1052] # FCAV = 1914x1052
REC = [1280,720] # BDD100K = 1280x720

# Set bin factor (nr of samples in each heat map point), dataset and 
# the boolean USE_ALL_OBJECTS which defines if all objects
# should be used or only cars.
BIN_FACTOR = 5.0
DATASET = 'BDD100K'
USE_ALL_OBJECTS = True

# Load csv file into numpy array
if USE_ALL_OBJECTS:
    FILE_PATH = './' + DATASET + '_all_classes_bb_centers.csv'
else:
    FILE_PATH = './' + DATASET + '_cars_bb_centers.csv'
bb_centers = np.genfromtxt(FILE_PATH, dtype=int, delimiter=',', skip_header=1)

# Create numpy matrix representing the image and load values into bins
xmax = int(round(REC[0]/BIN_FACTOR))
ymax = int(round(REC[1]/BIN_FACTOR))
bb_bins = np.zeros((ymax+1, xmax+1)) # numpy matrix = [y,x]
for box_center in bb_centers:
    x_indx = int(box_center[0]/BIN_FACTOR)
    y_indx = int(box_center[1]/BIN_FACTOR)
    bb_bins[y_indx,x_indx] = bb_bins[y_indx,x_indx] + 1

# Normalize values and plot heat map
bb_bins_norm = bb_bins/np.size(bb_bins)
bin_max_value = bb_bins_norm.max()
fig, ax = plt.subplots()
im = ax.imshow(bb_bins_norm,interpolation='bicubic')
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
cbar = ax.figure.colorbar(im, orientation='horizontal', shrink=1.0, ticks=[0, bin_max_value/2, bin_max_value], pad=0.1)
cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])
if USE_ALL_OBJECTS:
    cbar.ax.set_xlabel('Number of object instances')
    # Save plot
    plt.savefig('all_classes_heatmap_%s_binfactor_%s.png' % (DATASET,int(BIN_FACTOR)))

else:
    cbar.ax.set_xlabel('Number of car instances')
    # Save plot
    plt.savefig('cars_heatmap_%s_binfactor_%d.png' % (DATASET,int(BIN_FACTOR)))

plt.show()
