'''
Save dataset mean RGB values in csv file
Master thesis project Cybercom
Roger Kalliomaki
'''

import os
import numpy
from PIL import Image
import random

# Set dataset, nr of images and image folder path
DATASET = 'BDD100K'
USE_MAX_NR_IMAGES = True
MAX_NR_IMAGES = 1000
DATASET_PATH = './JPEGImages/'

# Function that returns mean rgb value of an image located in argument path
def get_mean_rgb_image(image_path):
    image = Image.open(image_path, 'r')
    width, height = image.size
    pixel_values = list(image.getdata())
    channels = 0
    if image.mode == 'RGB':
        channels = 3
    elif image.mode == 'RGBA':
        channels = 4
    elif image.mode == 'L':
        channels = 1
    else:
        print('Unknown mode: %s' % image.mode)
        return None       
    pixel_values = numpy.array(pixel_values).reshape((width, height, channels))
    if channels == 4:
        pixel_values = pixel_values[:,:,:3]
    return numpy.mean(pixel_values, axis=(0, 1)).astype(int)

# Function that goes throug all images in argument path and calls the function
# get_mean_rgb_image for all images in path. Saves mean rgb values for all
# images in csv file
def save_to_file(dataset_path):
    result_file = open('./'+DATASET+'_rgb_values.csv', 'w')
    result_file.write('R,G,B' + '\n')
    path, dirs, files = next(os.walk(dataset_path))
    random.shuffle(files)
    file_count = len(files)
    file_counter = 1
    for filename in files:
        if USE_MAX_NR_IMAGES:
            print('Processing file: %s/%s' % (file_counter,MAX_NR_IMAGES))
        else:
            print('Processing file: %s/%s' % (file_counter,file_count))
        mean_rgb = get_mean_rgb_image(dataset_path + filename)
        csv_string = '%d,%d,%d\n' % (mean_rgb[0],mean_rgb[1],mean_rgb[2])
        result_file.write(csv_string)
        file_counter += 1
        if USE_MAX_NR_IMAGES and MAX_NR_IMAGES == file_counter:
            break
    if file_count == 0:
        print('No files in path %s' % dataset_path)
        
if __name__ == "__main__":
    save_to_file(DATASET_PATH)
