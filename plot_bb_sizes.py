'''
Plot bounding box sizes saved in given csv file
Master thesis project Cybercom
Roger Kalliomaki
'''

import matplotlib.pyplot as plt
import numpy as np

# Set dataset and the boolean USE_ALL_OBJECTS which defines if
# all objects should be used or only cars.
DATASET = 'BDD100K'
USE_ALL_OBJECTS = True

# Load csv file into numpy array
if USE_ALL_OBJECTS:
    FILE_PATH = './' + DATASET + '_all_classes_bb_sizes.csv'
else:
    FILE_PATH = './' + DATASET + '_cars_bb_sizes.csv'
bb_sizes = np.genfromtxt(FILE_PATH, delimiter=",", dtype=int)

# Save widths and heights from numpy array wiht bounding box sizes
dx = []
dy = []
for dx_dy in bb_sizes:
    dx.append(dx_dy[0])
    dy.append(dx_dy[1])

# Plot scatter plot
plt.scatter(dx,dy,s=1)
plt.xlabel('Bounding Box Width [pixels]')
plt.ylabel('Bounding Box Height [pixels]')
plt.xlim(0,500)
plt.ylim(0,500)

# Save plot
if USE_ALL_OBJECTS:
    plt.savefig('all_classes_bb_sizes_%s.png' % (DATASET))
else:
    plt.savefig('cars_bb_sizes_%s.png' % (DATASET))


plt.show()
